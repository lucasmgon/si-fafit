<p align="center">
    <img src="https://gitlab.com/lucasmgon/si-fafit/-/raw/master/1%C2%BA%20Semestre/T%C3%A9cnicas%20de%20Programa%C3%A7%C3%A3o/images/c.png" alt="C logo" width="175" height="165">
</p>

<h2 align="center">Técnicas de Programação - Linguagem C</h2>

Material destinado para aprendizagem inicial de programação, utilizando a linguagem C.

## Requisitos

Para o desenvolvimento utilizei os softwares:

- **[Dev-C++](http://orwelldevcpp.blogspot.com/)**
- **[Geany](https://www.geany.org/)**
- **[GCC](https://gcc.gnu.org/)**

## Autor

* **[Lucas Matheus Gonçalves](https://br.linkedin.com/in/lucasmgon)**


## Licença

Esse projeto é licenciado sob a Licença GPL v3.0 - consulte o arquivo [LICENSE.md](https://gitlab.com/lucasmgon/si-fafit/-/blob/master/LICENSE) para obter detalhes.